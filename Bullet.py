import pygame

class Bullet(pygame.sprite.Sprite):
    """Class to manage bullets fired from the ship"""
    def __init__(self, settings, screen, ship):
        """Create a bullet at the ship's position"""
        super(Bullet, self).__init__()
        self.screen = screen        
        self.image = pygame.Surface([8,8])
        self.image.fill(pygame.Color(255, 255, 255, 255))
        self.rect = pygame.Rect(ship.rect.centerx, ship.rect.top, settings.bullet_width, settings.bullet_height)
        
    def update(self):
        self.rect.centery -= 1

    def draw(self):
        """Draw the ship at its current location."""
        self.screen.blit(self.image, self.rect) # draws the Ship image into the screen
